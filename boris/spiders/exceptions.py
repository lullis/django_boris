class SpiderCrawlError(Exception):
    pass


class CapableSpiderNotFound(Exception):
    pass
