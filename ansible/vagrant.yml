---
- hosts: all
  gather_facts: false
  become: true

  vars:
    - boris_database_name: boris_dev
    - boris_database_username: vagrant
    - boris_database_password: insecurepasswd_database
    - boris_app_secret_key: this_is_an_insecure_secret_key
    - boris_app_username: vagrant
    - boris_app_service_environment_file_path: /etc/boris.environment
    - boris_app_virtualenv_folder_path: /home/vagrant/.virtualenvs
    - boris_app_virtualenv_name: boris
    - boris_app_pypi_url: file:///home/vagrant/code/

    - rabbitmq_enabled: yes
    - rabbitmq_users:
        - user: admin
          password: admin
          vhost: /
          configure_priv: .*
          read_priv: .*
          write_priv: .*
          tags: administrator

  roles:
    - role: stouts.rabbitmq
    - role: ANXS.postgresql

  pre_tasks:
    - name: bootstrap python (for ansible)
      raw: test -e /usr/bin/python || (apt -y update && apt install -y python)
      changed_when: False

    - setup: # original ansible pre_tasks

    - name: install debconf utils
      apt:
        name: debconf-utils

    - name: set to generate locales
      debconf:
        name: locales
        question: locales/locales_to_be_generated
        value: en_US.UTF-8 UTF-8, de_DE.UTF-8 UTF-8
        vtype: multiselect

    - name: Set default locale to en_US.UTF-8
      debconf:
        name: locales
        question: locales/default_environment_locale
        value: en_US.UTF-8
        vtype: select

    - name: make environment file
      template:
        src: environment.j2
        dest: "{{ boris_app_service_environment_file_path }}"

    - name: add some lines to .bashrc
      lineinfile:
        name: /home/vagrant/.bashrc
        line: "{{ item }}"
      with_items:
        - "set -a && source {{ boris_app_service_environment_file_path }} && set +a"
        - "source {{ boris_app_virtualenv_folder_path }}/{{ boris_app_virtualenv_name }}/bin/activate"

  tasks:
    - name: ensure we have necessary folders
      tags: webapp, virtualenv, setup
      become_user: vagrant
      file:
        path: "{{ boris_app_virtualenv_folder_path }}"
        owner: vagrant
        state: directory
        mode: 0755

    - name: create postgresql role
      become_user: postgres
      postgresql_user:
        name: "{{ boris_database_username }}"
        password: "{{ boris_database_password }}"
        encrypted: true
        state: present
        role_attr_flags: LOGIN,SUPERUSER

    - name: create webapp database
      become_user: postgres
      postgresql_db:
        name: "{{ boris_database_name }}"
        owner: "{{ boris_database_username }}"
        encoding: UTF-8
        state: present

    - name: install package dependencies
      tags: webapp, packages, apt
      apt:
        name: "{{ item }}"
      with_items:
        - python3
        - python3-pip
        - python3-dev
        - libsasl2-dev
        - libldap2-dev
        - libssl-dev
        - virtualenv
        - gcc

    - name: install python package dependencies
      tags: webapp, python, pip
      pip:
        virtualenv: "{{ boris_app_virtualenv_folder_path }}/{{ boris_app_virtualenv_name }}"
        name: "{{ boris_app_pypi_url }}"
        virtualenv_python: python3
        editable: true
        state: forcereinstall
